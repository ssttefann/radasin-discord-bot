from commands.command import Command
from random import uniform, randrange

class Random(Command):
    def __init__(self):
        self.args = "[min:broj] [max:broj] ili [rec1:slova] [rec2:slova] ... [recN:slova]"
        self.help = "Daje random broj izmedju min i max ili random rec od navedenih N reci"

    async def run(self, arguments, message, store):
        if len(arguments) < 2:
            raise Exception

        try:
            minNum = float(arguments[0])
            maxNum = float(arguments[1])
        except:
            #  Not numbers
            await message.channel.send(arguments[randrange(0, len(arguments))])
            return


        if int(minNum) == minNum and int(maxNum) == maxNum:
            minNum = int(minNum)
            maxNum = int(maxNum)

            await message.channel.send(randrange(minNum, maxNum))
        else:
            await message.channel.send(round(uniform(minNum, maxNum), 2))