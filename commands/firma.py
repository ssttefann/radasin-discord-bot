from commands.command import Command
from selenium import webdriver

class Firma(Command):
    def __init__(self):
        self.args = ""
        self.help = "Dam ti predlog kako da nazoveš firmu u stilu SFRJ državnih firmi"
    async def run(self, arguments, message, store):
        await message.channel.send("Ček da mućnem glavom...")
        driver = webdriver.PhantomJS()
        driver.get("https://nothke.github.io/SocialistCompanyGenerator/")
        p_element = await driver.find_element_by_id(id_="p1")
        await driver.quit()
        await message.channel.send("Mogao bi da je nazoveš " + p_element.text)