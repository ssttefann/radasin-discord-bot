from commands.command import Command

class Cimni(Command):
    def __init__(self):
        self.args = ""
        self.help = "Napišem mention svakome ko se prijavio na tvoj !nadji"
    async def run(self, arguments, message, store):
        if message.author.id not in store["nadji"]["userToMsg"].keys():
            return

        msgId = store["nadji"]["userToMsg"][message.author.id].id
        msgData = store["nadji"]["msgData"][msgId]

        msgStr = "ALO!!!\n"
        if len(msgData["people"]) == 0:
            await message.channel.send("Cimaj mamu! Nemam koga da cimam, niko ti se nije prijavio još na !nadji")
            return
        for user in msgData["people"]:
            msgStr += f"<@!{user.id}> "

        await message.channel.send(msgStr)