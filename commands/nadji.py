from commands.command import Command

class Nadji(Command):
    def __init__(self, store):
        self.args = "[ime_igre:slova] [broj_ljudi:broj]"
        self.help = "Pomognem ti da skupiš ljude za igranje. Možeš imati samo jedan '!nadji' aktivan odjednom. Pozovi ponovo da ga resetuješ."

        if "nadji" not in store.keys():
            store["nadji"] = {}
        if "userToMsg" not in store["nadji"]:
            store["nadji"]["userToMsg"] = {}
        if "msgData" not in store["nadji"]:
            store["nadji"]["msgData"] = {}

    async def run(self, arguments, message, store):
        nadjiMsg = await message.channel.send(f"{message.author.name} traži {arguments[1]} ljudi za {arguments[0]}\nReaguj na ovu poruku da se prijaviš.")

        if message.author.id in store["nadji"]["userToMsg"].keys():
            await store["nadji"]["userToMsg"][message.author.id].delete()
        store["nadji"]["userToMsg"][message.author.id] = nadjiMsg
        store["nadji"]["msgData"][nadjiMsg.id] = { "people": [], "message": nadjiMsg, "author": message.author, "game": arguments[0], "numOfPeople": int(arguments[1]) }
