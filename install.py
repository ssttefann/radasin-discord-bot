import subprocess
import sys
print("Installing dependencies...")
subprocess.check_call([sys.executable, "-m", "pip", "install", "-r", "requirements.txt"])
print("------Done! If there were no errors, all dependencies listed in the requirements.txt file should be installed.")