"""
Finds all imports and aggregates them into the requirements.txt file.
IMPORTANT: RUN THIS FROM THE ENVIRONMENT YOU ARE USING FOR THIS PROJECT
            the script only detects what is currently installed in pip
"""

import pkg_resources

print("Making requirements.txt file...")
file = open("requirements.txt", "w")
file.writelines([f"{p.project_name}\n" for p in pkg_resources.working_set])
file.close()
print("Done!")