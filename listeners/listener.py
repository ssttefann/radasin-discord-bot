from abc import ABC

class Listener(ABC):
    """
    Logika prilikom obradjivanja dogadjaja koji se osluškuje.
    Ukoliko ga listener odbacuje treba vratiti False, ukoliko ga je obradio
    treba vratiti True.
    Napomena: Ako Listener vrati True, event se ne propagira dalje niz listener listu!
    """
    async def notify(self, data, store):
        return False