from listeners.listener import Listener

async def updateMessage(msg, store):
    #  Get distinct users
    dUsers = []
    pplList = "Za sada:\n"
    for react in msg.reactions:
        async for user in react.users():
            if user not in dUsers:
                dUsers.append(user)
                pplList += f"{len(dUsers)}. {user.name}\n"

    msgData = store["nadji"]["msgData"][msg.id]
    msgData["people"] = dUsers

    for i in range(len(dUsers), msgData["numOfPeople"]):
        pplList += f"{i+1}. ?\n"

    # Update message
    await msg.edit(
        content=f"{msgData['author'].name} traži {msgData['numOfPeople']} ljudi za {msgData['game']}\nFali mu još {msgData['numOfPeople'] - len(msgData['people'])}/{msgData['numOfPeople']}.\n{pplList}\nReaguj na ovu poruku da se prijaviš.")

class NadjiOptIn(Listener):
    async def notify(self, data, store):
        if "reaction" not in data.keys():
            return False
        if data["reaction"].message.id not in store["nadji"]["msgData"].keys():
            return False

        await updateMessage(data["reaction"].message, store)
        await data["user"].send(f"Opa! <@!{data['user'].id}> ti se prijavio na !nadji.")
        return True

class NadjiOptOut(Listener):
    async def notify(self, data, store):
        if "message" not in data.keys():
            return False
        if data["message"].id not in store["nadji"]["msgData"].keys():
            return False

        await updateMessage(data["message"], store)
        return True